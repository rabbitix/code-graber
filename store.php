<?php
define('LOCKFILE', __DIR__ . '/' . 'Lock.txt');
if (file_exists(LOCKFILE)) {

    if (file_get_contents(LOCKFILE) == '1') {
        die();
    }
} else {
    file_put_contents(LOCKFILE, '0');
    die();
}
#region basic
include "Telegram.php";
define('CODER', 372865109);
$token = '873681392:AAH-z5-BiDTX-7m22UQbh7TpWFQJBPywujw';
$bot = new Telegram($token);
function log_to_developer($txt)
{
    $content = array(
        'chat_id' => CODER,
        'text' => $txt,
        'disable_notification' => true
    );

    global $bot;
    $bot->sendMessage($content);

}

function start()
{
    log_to_developer('i started now');
    file_put_contents(LOCKFILE, '1');
}

function stop()
{
    log_to_developer('i stoped now!');
    file_put_contents(LOCKFILE, '0');
}

#endregion
#region db


start();


//$data_base_username = "pln8_alex";
$data_base_username = "pln8_pln8";
$data_base_passwors = "ALEX@alex1234";
//$data_base_passwors = "password";

$db_type = "mysql";
$table_name = 'gits';
require "Medoo.php";
$database = new Medoo(array(
    'database_type' => 'mysql',
    'database_name' => 'pln8_code',
    'server' => 'localhost',
    'username' => $data_base_username,
    'password' => $data_base_passwors,
    'charset' => 'utf8mb4',
    'collation' => 'utf8mb4_general_ci',
    'port' => 3306,
    'logging' => true
));

function create_db()
{
    global $database;
    $query = "create table if not exists gits 
(
    id          int auto_increment
        primary key,
    raw_url     varchar(200)  null,
    description text          null,
    sent        int default 0 null,
    lang        text          null,
    owner       text          null,
    url         text          null,
    g_id        varchar(200)  null,
    constraint gits_g_id_uindex
        unique (g_id),
    constraint raw_url
        unique (raw_url)
)CHARACTER SET utf8 COLLATE utf8_general_ci;";
    $database->query($query);

}

create_db();
function insert($raw_url, $description, $lang, $owner, $url, $g_id)
{
    global $database;
    return $database->insert('gits', [

        "raw_url" => $raw_url,
        "description" => $description,
        "lang" => $lang,
        "owner" => $owner,
        'url' => $url,
        'g_id' => $g_id
    ]);
}

function update_status($g_id)
{
    global $database;
    return $database->update('gits', [
        'sent' => 1
    ], [
        'g_id' => $g_id
    ]);
}

#endregion


log_to_developer('start new 5 pages');
for ($page = 1; $page < 6; $page++) {

    $url = "https://api.github.com/gists/public?page=$page&per_page=100";
//    $url = "https://api.github.com/gists/public";

    $git = curl_init();
    curl_setopt($git, CURLOPT_URL, $url);
    curl_setopt($git, CURLOPT_HTTPHEADER, array('User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0'));
    curl_setopt($git, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($git);
    $result = json_decode($result);
    curl_close($git);

    foreach ($result as $res) {
        $html_url = $res->html_url;
        $g_id = $res->id;
        $des = $res->description;
        $owner = $res->owner->login;
        $lang = '';
        $raw_url = '';
        foreach ($res->files as $file) {
            $lang = $file->language;
            $raw_url = $file->raw_url;
            insert($raw_url, $des, $lang, $owner, $html_url, $g_id);
            usleep(100);
        }

    }
    sleep(1);
    log_to_developer("$page  is done");

}

$all = $database->count($table_name);
$python_count = $database->count($table_name,
    ['lang' => 'Python']);
$php_count = $database->count($table_name,
    ['lang' => 'PHP']);
$js_count = $database->count($table_name,
    ['lang' => 'JavaScript']);
$pn_count = $database->count($table_name,
    ['lang' => 'Jupyter Notebook']);


$log =
    "_> all : $all\n_> Python : $python_count\n_> PHP : $php_count\n_> Jupyter Notebook : $pn_count\n_> js : $js_count\n";

log_to_developer($log);

stop();